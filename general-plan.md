# General Plan

- https://www.mindtools.com/pages/article/smart-goals.htm

1.Specific

- What do I want to accomplish?
- Why is this goal important?
- Who is involved?
- Where is it located?
- Which resources or limits are involved?

2.Measurable

- How much?
- How many?
- How will I know when it is accomplished?

3.Achievable

- How can I accomplish this goal?
- How realistic is the goal, based on other constraints, such as financial factors?

4.Relevant

A relevant goal can answer "yes" to these questions:

- Does this seem worthwhile?
- Is this the right time?
- Does this match our other efforts/needs?
- Am I the right person to reach this goal?
- Is it applicable in the current socio-economic environment?

5.Time-bound

- When?
- What can I do six months from now?
- What can I do six weeks from now?
- What can I do today?