# Ruby Lead

Lộ trình nâng cao kĩ năng đảm nhiệm vị trí ruby lead. 

## 1.Thực hành căn bản

- [x] Ruby Basic
- [x] Loop
- [x] Hash & Array
- [x] Standard IO
- [x] Block
- [x] Modules
- [x] Exceptions
- [x] Date & Time
- [x] File IO: text, binary, json, csv
- [x] OOP
- [x] Regex
- [x] Databases: simple, ActiveRecord
- [x] Email
- [x] Socket
- [x] Multithreading
- [x] Rake
- [x] Image : png, jpeg
- [x] TDD: test/unit, Rspec
- [x] Web base : Rack
- [x] Queue,job: Sidekiq

Project giai đoạn 1:

1. Problems solving with ruby. Xây dựng blog cung cấp các vấn đề thông thường và giải pháp
2. Thử nghiệm các lib thông dụng và tập hợp config
3. Xây dựng các lib thông dụng

## 2.Thực hành phổ thông
 
- [x] Ruby on Rails Web App
- [x] Ruby on Rails REST API
- [x] Sinatra REST API
- [x] Amazon services

Project giai đoạn 2: Xây dựng iome portal(ror)

## 3.Thực hành chuyên sâu

Project giai đoạn 3: Xây dựng kid portal(ror(api) + angular(admin) + reactjs(front internal) + expressjs(seo content) )

## 4.Thực hành sáng tạo

