# Tam thập nhi lập


Khổng Tử từng nói: “Ngô thập hữu ngũ nhi chí vu học, tam thập nhi lập, tứ thập nhi bất hoặc, ngũ thập nhi tri thiên mệnh, lục thập nhi nhĩ thuận, thất thập nhi tòng tâm sở dục bất du củ”.


30 tuổi là ngọn núi chia đôi dòng nước, chia đôi ngã rẽ của đời người. Con người ở độ tuổi này về cơ bản đã có thể xác lập được phương hướng phát triển cuộc đời mình. Họ cũng có thể dựa vào bản lĩnh của tự thân mà gánh vác những trách nhiệm mình cần đảm đương.

“Nhi lập” ở đây là lập thân, lập gia, lập nghiệp.

- **Lập thân** là xác lập nhân cách và sự tu dưỡng của bản thân. Điều này bao gồm việc tu dưỡng trong **tư tưởng và hàm dưỡng đạo đức**, bồi dưỡng **năng lực** và có thể **tự lực tự cường**. Trong đó sự tự cường là cái gốc lập thân, là yêu cầu cơ bản nhất khi mỗi người muốn đứng vững trong xã hội

- **Lập nghiệp** là xác lập công danh, sự nghiệp mà mình muốn theo đuổi. Người 30 tuổi nên có nghề nghiệp vững chắc. Dẫu theo đuổi bất kỳ công việc nào cũng cần có một năng lực nhất định. Nói theo cách hiện đại chính là có một sở trường về kỹ năng nào đó. Lập nghiệp là phương thức mưu sinh, là cơ sở để độc lập về kinh tế. Đây cũng là quá trình con người ắt phải trải qua để thực hiện giá trị nhân sinh.

- **Lập gia** là lập gia đình. Xã hội cạnh tranh mạnh mẽ ngày nay đã kéo dài tuổi lập gia đình của những người trẻ. Nhưng đứng từ góc độ sinh sôi nảy nở của nhân loại mà nói thì độ tuổi này lại rất phù hợp. Hơn nữa con người khi đến 30 tuổi sẽ hiểu rõ về hôn nhân và trách nhiệm. Ý nghĩa của gia đình là tổ ấm. Lập gia đình thì tâm hồn mới tìm được bến đỗ bình yên để ngơi nghỉ.

Còn về trình tự lập nghiệp và lập gia đình, do tình huống của mỗi người mỗi khác, nên chúng ta không phải câu nệ thứ tự trước sau, mà hai điều này đều tương trợ cho nhau.

## Đánh giá tình hình hiện tại

**1.Lập thân**

- Tư tưởng: không tập trung, vô kỉ luật
- Năng lực tự cường: trung bình

**2.Lập nghiệp**

- Sự nghiệp muốn theo đuổi: chưa xác định
- Công danh: kém

**3.Lập gia:**

- Đã lập gia
- Trách nhiệm: thấp

## Cần cải thiện

**1.Lập thân**

- Tư tưởng: xác định rõ mục tiêu, thiết lập kỉ luật, tập trung vào từng thứ một.
- Năng lực tự cường: đánh giá rõ điểm mạnh yếu hiện tại, phương pháp cải thiện
- Các yếu tố ảnh hưởng đến năng lực tự cường: tâm(tư tưởng), thân(sức khỏe), trí(kiến thức, sự sáng suốt), tài chính

**2.Lập nghiệp**

- Làm cực tốt công việc hiện tại
- Trải nghiệm bản thân với công việc mới: digital marketing, sales, quản trị doanh nghiệp, start up

**3.Lập gia**

- Dành nhiều thời gian hơn cho gia đình
- Làm việc nhà nhanh nhất có thể

## Kế hoạch cụ thể

### 1.Lập thân

#### Tư tưởng

[x] Xác định rõ mục tiêu : tự cường 40 tuổi, tu thân

**1.Thân**

- Không sát sinh - lìa tà hạnh - phóng sinh
- Không tà dâm - lìa tà hạnh - phạm hạnh
- Không trộm cắp - lìa trộm cướp - bố thí

**2.Khẩu**

- Không vọng ngữ:
	- Lìa ỷ ngữ(lời thêu dệt) - nói lời ngay thẳng
	- Lìa vọng ngữ(lời dối trá) - nói lời thành thật
	- Lìa lưỡng thiệt(trước sau bất nhất) - nói lời hòa giải
	- Lìa ác khẩu - nói lời dịu dàng

**3.Ý**

- Không uống rượu
 - Lìa tham dục - quán bất tịnh
 - Lìa sân nhuế - quán từ bi
 - Lìa tà kiến - quán nhân duyên

#### Năng lực tự cường

- Thân : cường thân, trị gia, an toàn tài chính, an toàn tài sản, an cư
- Khẩu: giao tế
- Ý: trí, nghiệp

#### Tăng thu nhập

- [Freelancer](freelancer-ways.md): freelancer.com, upwork.com, freelancerviet: gd0-nghiên cứu thị trường, gd1-solo, gd2-smallteam, gd3-offshore (mỗi giai đoạn là 1 năm)
- Tự doanh: gd0-kiến thức, gd1-nghiên cứu thị trường, gd2-lựa chọn, gd3-thử nghiệm, gd4-khởi nghiệp (mỗi giai đoạn là 1 năm)
- Liên kết: webdn, webbanhang
- Giảng dạy: các khóa học cấp tốc về sử dụng mã nguồn

Các mã nguồn cần focus để làm freelancer:

1. php: wordpress, laravel, magento
2. js : reactjs, angular, vuejs
3. nodejs: expressjs, sailsjs, nestjs

#### Nâng cao trình độ ngoại ngữ

**Tiếng anh 850 ( Ref : https://edu2review.com/reviews/cach-quy-doi-diem-tu-toeic-sang-ielts-la-nhu-the-nao-2746.html)**

- GD0 : nghiên cứu lộ trình, chi tiết thời gian biểu( 1 tháng )
- GD1 : 450 ( 3 tháng )
- GD2 : 650 ( 4 tháng )
- GD3 : 850 ( 4 tháng )

**Tiếng Trung viết, đọc, hiểu, giao tiếp căn bản ( Ref : https://toihoctiengtrung.com/ )**

- GD0 : nghiên cứu và lộ trình, chi tiết thời gian biểu( 1 tháng )
- GD1 : bảng chữ cái, từ vựng ( 3 tháng)
- GD2 : câu và ngữ pháp( 4 tháng )
- GD3 : nghe nói cơ bản( 4 tháng)
- GD4 : đọc tài liệu, viết ghi chú, luyện nghe nâng cao, giao tiếp văn phòng cơ bản ( 4 tháng )

**Tiếng Nhật N5 ( ref : http://gotiengviet.com.vn/tieng-nhat-n5-la-gi-lo-trinh-hoc-tieng-nhat-n5-hieu-qua-nhat/)**

- GD0 : nghiên cứu và lộ trình, chi tiết thời gian biểu( 1 tháng )
- GD1 : bảng chữ cái, từ vựng ( 3 tháng )
- GD2 : câu và ngữ pháp( 4 tháng )
- GD3 : nghe nói cơ bản( 4 tháng )
- GD4 : đọc tài liệu, viết ghi chú, luyện nghe nâng cao, giao tiếp văn phòng cơ bản ( 4 tháng)

### 2.Lập nghiệp

#### Làm cực tốt công việc hiện tại

[**Frontend Lead**](frontend-lead.md)

- GD0 : nghiên cứu lộ trình, chi tiết thời gian biểu
- GD1 : thực hành căn bản
- GD2 : thực hành phổ thông
- GD3 : thực hành chuyên sâu
- GD4 : thực hành sáng tạo

[**Ruby Lead**](ruby-lead.md)

- GD0 : nghiên cứu lộ trình, chi tiết thời gian biểu
- GD1 : thực hành căn bản
- GD2 : thực hành phổ thông
- GD3 : thực hành chuyên sâu
- GD4 : thực hành sáng tạo


**Architecture**

- GD0 : nghiên cứu lộ trình, chi tiết thời gian biểu
- GD1 : thực hành căn bản: database, linux, bash, automation
- GD2 : thực hành phổ thông : c, c++, go, java, c#
- GD3 : thực hành chuyên sâu : devops, aws
- GD4 : thực hành sáng tạo

#### Tự rèn luyện công việc mới

- Kiến thức tổng hợp về kinh doanh( 1 năm )
- Digital marketing ( 1 năm )
- Sales ( 1 năm )
- Quản trị doanh nghiệp ( 1 năm )
- Start up ( 1 năm )