# Architecture

1.Linux system

- [x] Minimum knownledge
- [ ] Advanced level
- [ ] Devops

2.Programming languages & Methodologies

- [ ] Basic Syntax
- [ ] Error Handles
- [ ] Common technical : Array, Hash, String, File, Standard IO
- [ ] Methodologies: procedural, OOP, functional
- [ ] Database Connection
- [ ] Web base

3.Database

- [ ] SQL Basic : SQLite3
- [ ] Relational Database Design: MySQL
- [ ] NoSQL Basic: MongoDB
- [ ] NoSQL Database Design : MongoDB
- [ ] Key,Value Database : Redis
- [ ] Search Engine Database : ElasticSearch

4.Frameworks

**Backend**:

- [] Strong and Rapid : Rails
- [] Light: SailsJS
- [] Common: Laravel
- [] Original: ExpressJS
- [] Easy and Rapid: Codeigniter

**Frontend**:

- [] Strong and Rapid : Angular
- [] Commmon: ReactJS, NextJS
- [] Light: VueJS
- [] Specification:

**Web**:

- Web Services
- Ecommerce
- Search
- SEO
- Socket
- Telegram

**CodeManagement, Queue Jobs, Dev ENV**

- GitHub/GitLab, 
- Docker containers
- Message Queuing, Apache Kafka

**Email,SMS,Payment**

- Twillio
- MailJet
- Paypal, Gocardless
- Nganluong
- SMSPortal: Vina,Mobi,Viettel

**Things to know**

- Regular Expressions
- SQL ( sequel)
- Debugging
- Scripting & Tooling

5.Software Development Methodology:

- Scrum : Read head first scrum
- https://dzone.com/articles/agile-framework-comparison-scrum-vs-kanban-vs-lean
- https://www.seguetech.com/waterfall-vs-agile-methodology/

6. System Design

7. Test

- Unit
- Functional
- Integration

8. Performance & Analytics

9. Deployment

10. Documenting

11. Clean Code

12. Cloud, Devops technical