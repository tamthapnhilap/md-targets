# Frontend Lead

Lộ trình nâng cao kĩ năng đảm nhiệm vị trí frontend lead. 

## 1.Thực hành căn bản

- [x] HTML5, CSS3
- [x] SASS, SCSS, LESS
- [x] Javascript, TypeScript
- [x] Webpack, Babel

Project giai đoạn 1:

1.Xây dựng các component sau: ( javascript )

- Giỏ hàng
- Form Khảo sát
- Thống kê analytic
- Responsive Slider
- Quiz: get data from json, post result on facebook, quick install on website

2.SinglePage Company Intro: (javascript)

- Navigation, Slider, About, Services, Clients & Testimonials, Portfolio, Team, Career, Contact, Footer, Posts( from facebook )
- Multilanguage
- Data from json
- Allow share facebook

3.Web bán hàng single page: ( typescript )

- Navigation
- Search
- Product list
- Cart
- https://developers.google.com/sheets/api/

4.Hackerank

## 2.Thực hành phổ thông
 
- [x] CSS Framework: Bootstrap, UIKit, Materialize, Pure, Custom
- [x] Javascript Framework : ReactJS, Vuejs
- [x] Angular: Angular, AngularJS
- [x] Javascript Lib: jquery, lodash, D3
- [x] NodeJS : ExpressJS, SailsJS
- [x] Database : Firebase, MongoDB

Xây dựng các component sau bằng reactjs + expressjs + mysql

1. Giỏ hàng
2. Form khảo sát
3. List nhạc yêu thích theo chủ đề
4. App đọc báo tổng hợp từ rss
5. App quản lí chi tiêu, đọc và lưu dữ liệu vào google drive sheet
6. App trắc nghiệm IQ, có post lên facebook, lấy data random từ json

Xây dựng trang web sau bằng angular + sailjs + mongodb

1. Phần mềm quản lí dữ liệu cá nhân
2. Phần mềm quản lí khách hàng
3. Phần mềm quản lí bán hàng theo module: shop, quán ăn, quán cà phê

Xây dựng phần mềm sau bằng vuejs + expressjs + firebase

1. Phần mềm quản lí file cá nhân( dùng local ko public)
2. Phần mềm tương tác với facebook( dùng local ko public )

## 3.Thực hành chuyên sâu

- [x] CSS Pattern : BEM, SMACSS or OOCSS
- [x] Javascript Pattern
- [x] Typescript Advanced
- [x] Test : Mocha, Jest
- [x] Code quality
- [x] Methodologies

## 4.Thực hành sáng tạo

- Architecture for applications with frameworks
- Speed up development process
- Built-in components
- Built-in scripts
- Common issues
